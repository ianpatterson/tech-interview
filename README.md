# Tech Interview



## Installation

The interview project will be conducted by the host. But, if you would like to run this project locally, the following is required:

- Python 3.8+
- Robot Framework 5.0
- Robot Framework Browser
- Google Chrome

To make it easier, if you already have Python installed, you can navigate to the project folder in a terminal and run `pip install -r requirements.txt`

## Running the Project

- Initiate the demo app by executing the script: `python demoapp/server.py`
- While this is running, the demo page can be accessed by navigating to `localhost:7272` in your browser
- Run the login tests with `robot login.robot`

## ???

More info needed?