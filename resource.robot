*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           Browser
Library           Collections
Library    test/lib/python3.8/site-packages/robot/libraries/Collections.py

*** Variables ***
${SERVER}         localhost:7272
${BROWSER}        chromium
${DELAY}          0
${VALID USER}     demo
${VALID PASSWORD}    mode
${LOGIN URL}      http://${SERVER}/
${WELCOME URL}    http://${SERVER}/welcome.html
${ERROR URL}      http://${SERVER}/error.html

*** Keywords ***
Open Browser To Login Page
    New Page    ${LOGIN URL}
    Login Page Should Be Open

Login Page Should Be Open
    Get Title    ==    Login Page

Go To Login Page
    Go To    ${LOGIN URL}
    Login Page Should Be Open

Input Username
    [Arguments]    ${username}
    Fill Text    input#username_field    ${username}

Input Password
    [Arguments]    ${password}
    Fill Text    input#password_field    ${password}

Submit Credentials
    Click    input#login_button

Welcome Page Should Be Open
    ${url}=    Get Url
    Should Contain    ${url}    ${WELCOME URL}
    Get Title    ==    Welcome Page

Login Should Have Failed
    ${url}=    Get Url
    Should Contain    ${url}    ${ERROR URL}
    Get Title    ==    Error Page

Sales Total for All Averages Should Match ${expected_total}
    [Documentation]    Validate that displayed averages are numerical values
    
    # Get all the rows in the sales column
    ${sales_rows}=    Get Elements    td.tg-sales
    
    # Create placeholder
    ${total}=    Set Variable    ${0}
    
    FOR    ${row}    IN    @{sales_rows}
        # Verify the value is a number and add to total
        ${value}=    Get Text    ${row}
        
        Should Match Regexp    ${value}    	\\d

        ${value}=    Convert To Integer    ${value}
        ${total}=    Evaluate    ${total} + ${value}
    END
        Log     ${total}

    Should Be Equal As Integers    ${total}    ${expected_total}