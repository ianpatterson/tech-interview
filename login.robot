*** Settings ***
Resource          resource.robot
Test Teardown     Close Browser

*** Variables ***
${EXPECTED_SALES_TOTAL}    394

*** Test Cases ***
Login And Validate Table Info
    Given browser is opened to login page
    #How can we get past the login?
    When user "demo" logs in with password "mode"
    Then welcome page should be open
    Then sales averages should be their expected values
    #Then test that dates are all in March. 


Invalid Login
    Given Browser is opened to login page
    When user "demo" logs in with password "fake"
    Then Login Should Have Failed

*** Keywords ***
Browser is opened to login page
    Open browser to login page

User "${username}" logs in with password "${password}"
    Input username    ${username}
    Input password    ${password}
    Submit credentials

Sales averages should be their expected values
    Sales Total for All Averages Should Match ${EXPECTED_SALES_TOTAL}